# GPU Programming with TNL

This repository contains example source codes and presentations for the Youtube channel GPU Programming with TNL.

## List of episodes

1. [Introduction and quick set up of TNL](https://www.youtube.com/watch?v=4ghHCqBKFHs&t=1s)
2. [Arrays and memory management](https://www.youtube.com/watch?v=4XkGrgdi95E&t=303s)
3. [Vectors and magical ET](https://www.youtube.com/watch?v=ogTKZdv8j7w)
4. [Parallel for and lambda functions](https://www.youtube.com/watch?v=50cgur3C_R4)
5. [Parallel reduction](https://www.youtube.com/watch?v=BT9XI6wITVA)

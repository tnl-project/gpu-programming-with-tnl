#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Vector.h>
#include <iostream>

template< typename Vector >
void
comparison( const Vector& u, const Vector& v )
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto v_view = v.getConstView();
   auto u_view = u.getConstView();
   auto fetch = [ = ] __cuda_callable__( Index i ) -> bool
   {
      return u_view[ i ] == v_view[ i ];
   };
   auto compare = TNL::Algorithms::reduce< Device >( 0, v.getSize(), fetch, TNL::LogicalAnd{} );

   std::cout << "u = " << u << std::endl;
   std::cout << "v = " << v << std::endl;
   std::cout << ( compare ? "u is equal to v." : "u is not equal to v." ) << std::endl;
}

int
main( int argc, char** argv )
{
   TNL::Containers::Vector< int, TNL::Devices::Host > u_host{ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, v_host{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CPU:" << std::endl;
   comparison( u_host, v_host );

#ifdef __CUDACC__
   TNL::Containers::Vector< int, TNL::Devices::Cuda > u_gpu{ 1, 2, 3, 4, 5, -6, -7, -8, -9 },
      v_gpu{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CUDA GPU:" << std::endl;
   comparison( u_gpu, v_gpu );
#endif

   return EXIT_SUCCESS;
}
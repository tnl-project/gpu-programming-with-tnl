#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Vector.h>
#include <iostream>

template< typename Vector >
void
abs_summation( const Vector& v )
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto v_view = v.getConstView();
   auto fetch = [ = ] __cuda_callable__( Index i )
   {
      return TNL::abs( v_view[ i ] );
   };
   auto abs_sum = TNL::Algorithms::reduce< Device >( 0, v.getSize(), fetch, TNL::Plus{} );

   std::cout << "v = " << v << std::endl;
   std::cout << "Summation of absolute values of v = " << abs_sum << std::endl;
}

int
main( int argc, char** argv )
{
   TNL::Containers::Vector< int, TNL::Devices::Host > v_host{ 1, 2, 3, 4, 5, -6, -7, -8, -9 };
   std::cout << "Testing parallel reduction on CPU:" << std::endl;
   abs_summation( v_host );

#ifdef __CUDACC__
   TNL::Containers::Vector< int, TNL::Devices::Cuda > v_gpu{ 1, 2, 3, 4, 5, -6, -7, -8, -9 };
   std::cout << "Testing parallel reduction on CUDA GPU:" << std::endl;
   abs_summation( v_gpu );
#endif

   return EXIT_SUCCESS;
}
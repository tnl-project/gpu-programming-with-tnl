#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Vector.h>
#include <iostream>

template< typename Vector >
void
get_arg_max( const Vector& v )
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto v_view = v.getConstView();
   auto fetch = [ = ] __cuda_callable__( int i )
   {
      return v_view[ i ];
   };
   auto reduction = [] __cuda_callable__( int& a, const int b, int& aIdx, const int bIdx )
   {
      if( a < b ) {
         a = b;
         aIdx = bIdx;
      }
      else if( a == b && bIdx < aIdx )
         aIdx = bIdx;
   };
   auto arg_max =
      TNL::Algorithms::reduceWithArgument< Device >( 0, v.getSize(), fetch, reduction, std::numeric_limits< int >::lowest() );
   std::cout << "v = " << v << std::endl;
   std::cout << "max value = " << arg_max.first << " at position " << arg_max.second << std::endl;
}

int
main( int argc, char** argv )
{
   TNL::Containers::Vector< int, TNL::Devices::Host > v_host{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CPU:" << std::endl;
   get_arg_max( v_host );

#ifdef __CUDACC__
   TNL::Containers::Vector< int, TNL::Devices::Cuda > v_gpu{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CUDA GPU:" << std::endl;
   get_arg_max( v_gpu );
#endif

   return EXIT_SUCCESS;
}
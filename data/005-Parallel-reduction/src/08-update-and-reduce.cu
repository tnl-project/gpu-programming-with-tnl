#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Vector.h>
#include <TNL/Timer.h>
#include <iostream>

template< typename Vector >
void
update_and_reduce( const Vector& u, Vector& v )
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto u_view = u.getConstView();
   auto v_view = v.getView();
   TNL::Timer timer;

   // Separated update and reduce
   Vector w( u );
   double total_update = 0;
   timer.reset();
   timer.start();
   for( int loop = 0; loop < 5; loop++ ) {
      w = 0.5 * ( u + v );
      v += w;
      total_update += sum( TNL::abs( w ) );
   }
   timer.stop();
   auto time_1 = timer.getRealTime();

   std::cout << "Time for separated update and reduce: " << time_1 << " s" << std::endl;

   // Fused update and reduce
   v = 0;
   total_update = 0;
   timer.reset();
   timer.start();
   for( int loop = 0; loop < 5; loop++ ) {
      total_update += TNL::Algorithms::reduce< Device >(
         0,
         u.getSize(),
         [ = ] __cuda_callable__( Index i ) mutable
         {
            double w_i = 0.5 * ( u_view[ i ] + v_view[ i ] );
            v_view[ i ] += w_i;
            return TNL::abs( w_i );
         },
         TNL::Plus{} );
   }
   timer.stop();
   auto time_2 = timer.getRealTime();
   std::cout << "Time for fused update and reduce: " << time_2 << " s" << std::endl;
   std::cout << "Speedup: " << time_1 / time_2 << std::endl;
}

int
main( int argc, char** argv )
{
   const int size = 1 << 24;
   TNL::Containers::Vector< double, TNL::Devices::Host > u_host( size, 1 ), v_host( size, 0 );
   std::cout << "Testing parallelFor on CPU:" << std::endl;
   update_and_reduce( u_host, v_host );

#ifdef __CUDACC__
   TNL::Containers::Vector< double, TNL::Devices::Cuda > u_gpu( size, 1 ), v_gpu( size, 0 );
   std::cout << "Testing parallelFor on CUDA GPU:" << std::endl;
   update_and_reduce( u_gpu, v_gpu );
#endif

   return EXIT_SUCCESS;
}
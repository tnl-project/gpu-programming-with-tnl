#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Vector.h>
#include <iostream>

template< typename Vector >
void
contains( const Vector& v, int value )
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto v_view = v.getConstView();
   auto fetch = [ = ] __cuda_callable__( Index i ) -> bool
   {
      return v_view[ i ] == value;
   };
   auto cont = TNL::Algorithms::reduce< Device >( 0, v.getSize(), fetch, TNL::LogicalOr{} );

   std::cout << "v = " << v << std::endl;
   std::cout << "value = " << value << std::endl;
   std::cout << ( cont ? "v contains " : "v does not contain " ) << value << std::endl;
}

int
main( int argc, char** argv )
{
   TNL::Containers::Vector< int, TNL::Devices::Host > v_host{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CPU:" << std::endl;
   contains( v_host, 5 );

#ifdef __CUDACC__
   TNL::Containers::Vector< int, TNL::Devices::Cuda > v_gpu{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   std::cout << "Testing parallel reduction on CUDA GPU:" << std::endl;
   contains( v_gpu, 5 );
#endif

   return EXIT_SUCCESS;
}
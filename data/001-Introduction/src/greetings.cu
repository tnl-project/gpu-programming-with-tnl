#include <TNL/Algorithms/parallelFor.h>
#include <cstdlib>

int main( int argc, char* argv[] )
{
   TNL::Algorithms::parallelFor< TNL::Devices::Cuda >( 1, 4,
      [] __cuda_callable__ ( int i ) {
         printf( "%d. Greetings from your GPU. \n", i );
      });
   return EXIT_SUCCESS;
}
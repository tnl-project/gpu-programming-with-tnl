#include <TNL/Devices/Host.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <iostream>

template <typename Device> void addElements() {
  TNL::Matrices::DenseMatrix<double, Device> matrix(5, 5);

  for (int row = 0; row < 5; row++)
    matrix.setElement(row, row, 2.0);

  std::cout << "Initial matrix is: " << std::endl << matrix << std::endl;

  for (int row = 0; row < 5; row++)
    for (int column = 0; column < 5; column++)
      matrix.addElement(row, column, 1.0, 3.0);

  std::cout << "Matrix after addition is: " << std::endl << matrix << std::endl;
}

int main(int argc, char *argv[]) {
  std::cout << "Add elements on host:" << std::endl;
  addElements<TNL::Devices::Host>();

#ifdef __CUDACC__
  std::cout << "Add elements on CUDA device:" << std::endl;
  addElements<TNL::Devices::Cuda>();
#endif
}
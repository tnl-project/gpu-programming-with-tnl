#include <iostream>
#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void
matrixProductExample()
{
   TNL::Matrices::DenseMatrix< double, Device > A;
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > B = {
      { 5, 6, 6, 2 },
      { 6, 5, 7, 4 },
      { 7, 2, 1, 8 },
      { 3, 3, 9, 1 } };
   // clang-format on

   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > C = {
      { 4, 3, 2, 6 },
      { 3, 4, 2, 8 },
      { 1, 4, 7, 9 },
      { 2, 3, 8, 0 } };
   // clang-format on

   std::cout << "The matrix B reads as:" << std::endl << B << std::endl;
   std::cout << "The matrix C reads as:" << std::endl << C << std::endl;

   A.getMatrixProduct( B, C );
   std::cout << "Product A=B*C: " << std::endl << A << std::endl;

   A.getMatrixProduct( B, C, 1.0, TNL::Matrices::TransposeState::Transpose );
   std::cout << "Product A=B^T*C: " << std::endl << A << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Multiplying matrices on CPU ... " << std::endl;
   matrixProductExample< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Multipying matrices on CUDA GPU ... " << std::endl;
   matrixProductExample< TNL::Devices::Cuda >();
#endif
}

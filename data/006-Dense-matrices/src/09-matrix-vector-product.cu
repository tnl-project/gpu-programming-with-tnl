#include <TNL/Devices/Cuda.h>
#include <TNL/Devices/Host.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <functional>
#include <iomanip>
#include <iostream>

template< typename Device >
void
vectorProduct()
{
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > A {
      {  1,  0,  0,  0,  0 },
      {  1,  2,  0,  0,  0 },
      {  0,  1,  8,  0,  0 },
      {  0,  0,  1,  9,  0 },
      {  0,  0,  0,  0,  1 } };
   // clang-format on

   TNL::Containers::Vector< double, Device > x( A.getColumns() ), y( A.getRows() );
   x = 1.0;

   std::cout << "The matrix A reads as:" << std::endl << A << std::endl;
   std::cout << "The vector x equals:" << x << std::endl;

   A.vectorProduct( x, y );
   std::cout << "Result of A * x is: " << y << std::endl;

   A.vectorProduct( x, y, 0.5, 1.0 );
   std::cout << "Result of 0.5 * A * x + 1.0 * y is: " << y << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Matrix-vector multiplication on host:" << std::endl;
   vectorProduct< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << std::endl;
   std::cout << "Matrix-vector multiplication on CUDA device:" << std::endl;
   vectorProduct< TNL::Devices::Cuda >();
#endif
}
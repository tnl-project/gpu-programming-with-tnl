#include <iostream>
#include <iomanip>
#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>
#include <TNL/Timer.h>

using Coordinates = TNL::Containers::StaticArray< 2, int >;

int
main( int argc, char* argv[] )
{
   std::cout << "Benchmarking setup of dense matrix:" << std::endl;

   std::cout << std::setw( 10 ) << "Size" << std::setw( 20 ) << "CPU" << std::setw( 20 ) << "Copy to GPU" << std::setw( 20 )
             << "setElement GPU" << std::setw( 20 ) << "paralellFor GPU" << std::setw( 20 ) << "forElements GPU" << std::endl;
   std::cout << std::setfill( '-' ) << std::setw( 110 ) << "" << std::endl;
   std::cout << std::setfill( ' ' );
   TNL::Timer timer;
   for( int size : { 10, 100, 500, 1000, 5000, 10000 } ) {
      TNL::Matrices::DenseMatrix< float, TNL::Devices::Host > matrixHost( size, size );
      timer.start();
      for( int row = 0; row < size; row++ )
         for( int column = 0; column < size; column++ )
            matrixHost.setElement( row, column, 1.0 );
      timer.stop();
      auto timeHost = timer.getRealTime();

      double timeCopyToGPU( -1.0 ), timeSetElementGPU( -1.0 ), timeParallelForGPU( -1.0 ), timeForElementsGPU( -1.0 );
#ifdef __CUDACC__
      TNL::Matrices::DenseMatrix< float, TNL::Devices::Cuda > matrixCuda( size, size );
      timer.reset();
      timer.start();
      matrixCuda = matrixHost;
      timer.stop();
      timeCopyToGPU = timer.getRealTime();

      timer.reset();
      timer.start();
      for( int row = 0; row < size; row++ )
         for( int column = 0; column < size; column++ )
            matrixCuda.setElement( row, column, 1.0 );
      timer.stop();
      timeSetElementGPU = timer.getRealTime();

      timer.reset();
      timer.start();
      Coordinates begin{ 0, 0 }, end{ size, size };
      auto matrixCudaView = matrixCuda.getView();
      TNL::Algorithms::parallelFor< TNL::Devices::Cuda >( begin,
                                                          end,
                                                          [ = ] __cuda_callable__( const Coordinates& coordinates ) mutable
                                                          {
                                                             matrixCudaView.setElement(
                                                                coordinates[ 0 ], coordinates[ 1 ], 1.0 );
                                                          } );
      timer.stop();
      timeParallelForGPU = timer.getRealTime();

      timer.reset();
      timer.start();
      matrixCuda.forAllElements(
         [ = ] __cuda_callable__( int rowIdx, int columnIdx, int globalIdx, float& value )
         {
            value = 1.0;
         } );
      timer.stop();
      timeForElementsGPU = timer.getRealTime();
#endif
      std::cout << std::setw( 10 ) << size << std::setw( 20 ) << timeHost << std::setw( 20 ) << timeCopyToGPU << std::setw( 20 )
                << timeSetElementGPU << std::setw( 20 ) << timeParallelForGPU << std::setw( 20 ) << timeForElementsGPU
                << std::endl;
   }
}
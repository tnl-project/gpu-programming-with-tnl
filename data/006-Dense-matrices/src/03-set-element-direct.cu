#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/StaticArray.h>
#include <TNL/Devices/Host.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <iostream>

template< typename Device >
void
setElements()
{
   using Coordinates = TNL::Containers::StaticArray< 2, int >;
   TNL::Matrices::DenseMatrix< double, Device > matrix( 5, 5 );
   auto matrixView = matrix.getView();

   auto f = [ = ] __cuda_callable__( const Coordinates& coordinates ) mutable
   {
      const int& rowIdx = coordinates[ 0 ];
      const int& columnIdx = coordinates[ 1 ];
      const double value = 5.0 - abs( rowIdx - columnIdx );
      matrixView.setElement( rowIdx, columnIdx, value );
   };
   Coordinates begin{ 0, 0 }, end{ 5, 5 };
   TNL::Algorithms::parallelFor< Device >( begin, end, f );
   std::cout << "Matrix reads as: " << std::endl << matrix << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Set elements on host:" << std::endl;
   setElements< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Set elements on CUDA device:" << std::endl;
   setElements< TNL::Devices::Cuda >();
#endif
}
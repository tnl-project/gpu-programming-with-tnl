#include <TNL/Devices/Cuda.h>
#include <TNL/Devices/Host.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <functional>
#include <iomanip>
#include <iostream>

template< typename Device >
void
reduceRows()
{
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > matrix {
      {  1,  0,  0,  0,  0 },
      {  1,  2,  0,  0,  0 },
      {  0,  1,  8,  0,  0 },
      {  0,  0,  1,  9,  0 },
      {  0,  0,  0,  0,  1 } };
   // clang-format on

   TNL::Containers::Vector< double, Device > x( matrix.getColumns() ), y( matrix.getRows() );
   x = 1.0;

   auto xView = x.getView();
   auto yView = y.getView();

   auto fetch = [ = ] __cuda_callable__( int rowIdx, int columnIdx, const double& value ) -> double
   {
      return xView[ columnIdx ] * value;
   };

   auto keep = [ = ] __cuda_callable__( int rowIdx, const double& value ) mutable
   {
      yView[ rowIdx ] = value;
   };

   matrix.reduceRows( 0, matrix.getRows(), fetch, TNL::Plus{}, keep );

   std::cout << "The matrix reads as:" << std::endl << matrix << std::endl;
   std::cout << "The input vector is:" << x << std::endl;
   std::cout << "Result of matrix-vector multiplication is: " << y << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Rows reduction on host:" << std::endl;
   reduceRows< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << std::endl;
   std::cout << "Rows reduction on CUDA device:" << std::endl;
   reduceRows< TNL::Devices::Cuda >();
#endif
}
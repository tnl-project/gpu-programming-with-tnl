#include <iostream>
#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void
matrixTransposition()
{
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > A = {
      { 5, 0, 0, 0 },
      { 6, 5, 0, 0 },
      { 7, 2, 1, 0 },
      { 3, 3, 9, 1 } };
   // clang-format on

   std::cout << "The matrix A reads as:" << std::endl << A << std::endl;

   TNL::Matrices::DenseMatrix< double, Device > B;
   B.getTransposition( A );
   std::cout << "The matrix B = A^T reads as:" << std::endl << B << std::endl;

   A.getInPlaceTransposition();
   std::cout << "The matrix A^T reads as:" << std::endl << B << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Matrix transposition on CPU ... " << std::endl;
   matrixTransposition< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Matrix transposition on CUDA GPU ... " << std::endl;
   matrixTransposition< TNL::Devices::Cuda >();
#endif
}

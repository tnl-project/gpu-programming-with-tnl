#include <TNL/Devices/Host.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <functional>
#include <iomanip>
#include <iostream>

template< typename Device >
void
reduceRows()
{
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > matrix {
      {  1,  0,  0,  0,  0 },
      {  1,  2,  0,  0,  0 },
      {  0,  1,  8,  0,  0 },
      {  0,  0,  1,  9,  0 },
      {  0,  0,  0,  0,  1 } };
   // clang-format on

   TNL::Containers::Vector< double, Device > rowMax( matrix.getRows() );
   auto rowMaxView = rowMax.getView();

   auto fetch = [ = ] __cuda_callable__( int rowIdx, int columnIdx, const double& value ) -> double
   {
      return TNL::abs( value );
   };

   auto keep = [ = ] __cuda_callable__( int rowIdx, const double& value ) mutable
   {
      rowMaxView[ rowIdx ] = value;
   };

   matrix.reduceAllRows( fetch, TNL::Max{}, keep );

   std::cout << "Max. elements in rows are: " << rowMax << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Rows reduction on host:" << std::endl;
   reduceRows< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Rows reduction on CUDA device:" << std::endl;
   reduceRows< TNL::Devices::Cuda >();
#endif
}
#include <iostream>
#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void
matrixAddition()
{
   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > A = {
      { 5, 6, 6, 2 },
      { 6, 5, 7, 4 },
      { 7, 2, 1, 8 },
      { 3, 3, 9, 1 } };
   // clang-format on

   // clang-format off
   TNL::Matrices::DenseMatrix< double, Device > B = {
      { 4, 3, 2, 6 },
      { 3, 4, 2, 8 },
      { 1, 4, 7, 9 },
      { 2, 3, 8, 0 } };
   // clang-format on

   std::cout << "The matrix A reads as:" << std::endl << A << std::endl;
   std::cout << "The matrix B reads as:" << std::endl << B << std::endl;

   A.addMatrix( B );

   std::cout << "Addition A+B: " << std::endl << A << std::endl;

   A.addMatrix( B, 1.0, 1.0, TNL::Matrices::TransposeState::Transpose );

   std::cout << "Addition A+B+B^T: " << std::endl << A << std::endl;
}

int
main( int argc, char* argv[] )
{
   std::cout << "Matrix addition on CPU ... " << std::endl;
   matrixAddition< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Matrix addition on CUDA GPU ... " << std::endl;
   matrixAddition< TNL::Devices::Cuda >();
#endif
}

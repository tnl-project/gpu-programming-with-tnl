#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void et_element_wise_comparisons()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ -2, -1, 0, 1, 2 }, y{ 2, 1, 0, 1, 2 };
   std::cout << "x =                                     " << x << std::endl;
   std::cout << "y =                                     " << y << std::endl;
   std::cout << "===========================================================" << std::endl;
   std::cout << "equalTo( x, 0 ) ->                      " << TNL::equalTo( x, 0 ) << std::endl;
   std::cout << "equalTo( x, y ) ->                      " << TNL::equalTo( x, y ) << std::endl;
   std::cout << "notEqualTo( x, y ) ->                   " << TNL::notEqualTo( x, y ) << std::endl;
   std::cout << "less( x, y ) ->                         " << TNL::less( x, y ) << std::endl;
   std::cout << "greater( x, 0 ) ->                      " << TNL::greater( x, 0 ) << std::endl;
   std::cout << "greater( x, y ) ->                      " << TNL::greater( x, y ) << std::endl;
   std::cout << "lessEqual( x, 0 ) ->                    " << TNL::lessEqual( x, 0 ) << std::endl;
   std::cout << "lessEqual( x, y ) ->                    " << TNL::lessEqual( x, y ) << std::endl;
   std::cout << "greaterEqual( x, 0 ) ->                 " << TNL::greaterEqual( x, 0 ) << std::endl;
   std::cout << "greaterEqual( x, y ) ->                 " << TNL::greaterEqual( x, y ) << std::endl;
   std::cout << "greaterEqual( x, y ) && less( x, 2 ) -> " << ( TNL::greaterEqual( x, y ) && TNL::less( x, 2 ) ) << std::endl;
   std::cout << "x * less( x, 0 ) ->                     " << x * TNL::less( x, 0 ) << std::endl;
   std::cout << "===========================================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET and element-wise comparison on the CPU:" << std::endl;
   et_element_wise_comparisons< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Demonstrating ET and element-wise comparison on the CUDA GPU:" << std::endl;
   et_element_wise_comparisons< TNL::Devices::Cuda >();
#endif
}
#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>
#include <iomanip>

template< typename Device >
void et_functions()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ -4, -2, 0, 2, 4 };
   std::cout << std::setprecision( 4 );
   std::cout << "x =                                                  " << x << std::endl;
   std::cout << "========================================================================================" << std::endl;
   std::cout << "abs( x ) =                                           " << abs( x ) << std::endl;
   std::cout << "sin( TNL::pi / 4 * x ) =                             " << sin( TNL::pi / 4 * x ) << std::endl;
   std::cout << "cos( TNL::pi / 4 * x ) =                             " << cos( TNL::pi / 4 * x ) << std::endl;
   std::cout << "exp( x * x * sign( x ) ) =                           " << exp( x * x * sign( x ) ) << std::endl;
   std::cout << "sign( sin( x * exp( sqrt( abs( x * x * x ) ) ) ) ) = " << sign( sin( x * exp( sqrt( abs( x * x * x ) ) ) ) ) << std::endl;
   std::cout << "========================================================================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET and mathematical on the CPU:" << std::endl;
   et_functions< TNL::Devices::Host >();

#ifdef __CUDACC__
std::cout << "Demonstrating ET and mathematical on the CUDA GPU:" << std::endl;
   et_functions< TNL::Devices::Cuda >();
#endif
}
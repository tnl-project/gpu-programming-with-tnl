#include <TNL/Containers/StaticVector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

int main( int argc, char* argv[] )
{
   using Vector = TNL::Containers::StaticVector< 4, float >;
   Vector x{ 1, 1, 0, 0 }, y{ 2, 2, 2, 2 },z{ 0, 0, 0, 0 };
   std::cout << "=================================" << std::endl;
   std::cout << "x =            " << x << std::endl;
   std::cout << "y =            " << y << std::endl;
   std::cout << "z =            " << z << std::endl;
   std::cout << "=================================" << std::endl;
   std::cout << "x + y =        " << x + y << std::endl;
   std::cout << "x - y =        " << x - y << std::endl;
   std::cout << "x * y =        " << x * y << std::endl;
   std::cout << "x / y =        " << x / y << std::endl;

   z -= y;
   std::cout << "z -=  y -> z = " << z << std::endl;
   z *= y;
   std::cout << "z *=  y -> z = " << z << std::endl;
   z *= -1;
   std::cout << "z *= -1 -> z = " << z << std::endl;
   z /= -y;
   std::cout << "z /= -y -> z = " << z << std::endl;
   z += 3;
   std::cout << "z +=  3 -> z = " << z << std::endl;
   std::cout << "=================================" << std::endl;
}
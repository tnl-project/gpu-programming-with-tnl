#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

using namespace std;

template< typename Device >
void et_comparisons()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ -2, -1, 0, 1, 2 }, y{ 2, 1, 0, 1, 2 };
   cout << "x =                            " << x << endl;
   cout << "y =                            " << y << endl;
   cout << "abs( x ) =                     " << abs( x ) << endl;
   cout << "x * x =                        " << x * x << endl;
   cout << "sign( y ) =                    " << sign( y ) << endl;
   cout << "sign( x * x ) =                " << sign( x * x ) << endl;
   cout << "==================================================" << endl;
   cout << "y == x ->                      " << ( y == x ? "true" : "false" ) << endl;
   cout << "y == abs( x ) ->               " << ( y == abs( x ) ? "true" : "false" ) << endl;
   cout << "sign( y ) == sign( x * x ) ->  " << ( sign( y ) == sign( x * x ) ? "true" : "false" ) << endl;
   cout << "===================================================" << endl;
}

int main( int argc, char* argv[] )
{
   cout << "Demonstrating comparison of ET and vectors on the CPU:" << endl;
   et_comparisons< TNL::Devices::Host >();

#ifdef __CUDACC__
   cout << "Demonstrating comparison of ET and vectors on the CUDA GPU:" << endl;
   et_comparisons< TNL::Devices::Cuda >();
#endif
}
#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void et_and_reductions()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ -2, -1, 0, 1, 2 }, y(5, -1 );
   std::cout << "=============================================================" << std::endl;
   std::cout << "x = " << x << std::endl;
   std::cout << "y = " << y << std::endl;
   std::cout << "greater( x, y ) = " << greater( x, y ) << std::endl;
   std::cout << "greater( x, 0 ) = " << greater( x, 0 ) << std::endl;
   std::cout << "x * greater( x, 0 ) = " << x * greater( x, 0 ) << std::endl;
   std::cout << "=============================================================" << std::endl;
   std::cout << "Are all elements of x greater then those of y:          " << ( TNL::all( greater( x, y ) ) ? "yes" : "no" ) << std::endl;
   std::cout << "Is any element of x greater then those of y:            " << ( TNL::any( greater( x, y ) ) ? "yes" : "no" ) << std::endl;
   std::cout << "How many elements of x are greater then those of y:     " << sum( greater( x, y ) ) << std::endl;
   std::cout << "Which is the first element of x greater then that of y: " << argMax( greater( x, y ) ).second << std::endl;
   std::cout << "What is the sum of all positive elements of x:          " << sum( x * ( greater( x, 0  ) ) ) << std::endl;
   std::cout << "=============================================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET on the CPU:" << std::endl;
   et_and_reductions< TNL::Devices::Host >();

#ifdef __CUDACC__
std::cout << "Demonstrating ET on the CUDA GPU:" << std::endl;
   et_and_reductions< TNL::Devices::Cuda >();
#endif
}
#include <iostream>
#include <TNL/Containers/VectorView.h>

int main( int argc, char* argv[] )
{
   // Allocate vectors using operator new
   const int size = 10;
   float* x_data = new float[ size ];
   float* y_data = new float[ size ];
   float* z_data = new float[ size ];
   float* w_data = new float[ size ];
   {
      // Create an ArrayView wrapping the data
      TNL::Containers::VectorView< float > x( x_data, size );
      TNL::Containers::VectorView< float > y( y_data, size );
      TNL::Containers::VectorView< float > z( z_data, size );
      TNL::Containers::VectorView< float > w( w_data, size );

      // Evaluate w = x + 2*y + 3*z
      x = y = z = 1.0;
      w = x + 2*y + 3*z;

      // Print the result
      std::cout << " x = " << x << std::endl;
      std::cout << " y = " << y << std::endl;
      std::cout << " z = " << z << std::endl;
      std::cout << " w = " << w << std::endl;
   }
   // The data was not deallocated by the ArrayView, so we can still work with it.
   std::cout << " w_data[ 0 ] = " << w_data[ 0 ] << std::endl;

   // Do not forget to free the vectors
   delete[] x_data;
   delete[] y_data;
   delete[] z_data;
   delete[] w_data;
}
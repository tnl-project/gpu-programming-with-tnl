#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void reductions()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   using BooleanVector = TNL::Containers::Vector< bool, Device >;
   Vector x{ -2, -1, 0, 1, 2 }, y(5, 1 );
   BooleanVector b{ true, false, true, false, true };
   std::cout << "==============================================" << std::endl;
   std::cout << "x =                        " << x << std::endl;
   std::cout << "y =                        " << y << std::endl;
   std::cout << "sum( y ) =                 " << sum( y ) << std::endl;
   std::cout << "sum( abs( x ) ) =          " << sum( abs( x ) ) << std::endl;
   std::cout << "min( x ) =                 " << min( x ) << std::endl;
   std::cout << "max( x ) =                 " << max( x ) << std::endl;
   std::cout << "Scalar product: (x,y) =    " << ( x, y ) << std::endl;
   std::cout << "Scalar product: dot(x,y) = " << dot( x, y ) << std::endl;
   std::cout << "all( b ) =                 " << ( ( TNL::all( b ) ) ? "true" : "false" ) << std::endl;
   std::cout << "any( b ) =                 " << ( ( TNL::any( b ) ) ? "true" : "false" ) << std::endl;
   std::cout << "==============================================" << std::endl;
   auto [ x_min, i_min ] = argMin( x );
   std::cout << "Lowest element of x is " << x_min << " at position " << i_min << std::endl;
   auto [ x_max, i_max ] = argMax( x );
   std::cout << "Largest element of x is " << x_max << " at position " << i_max << std::endl;
   std::cout << "==============================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET on the CPU:" << std::endl;
   reductions< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Demonstrating ET on the CUDA GPU:" << std::endl;
   reductions< TNL::Devices::Cuda >();
#endif
}
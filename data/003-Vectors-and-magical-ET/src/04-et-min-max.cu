#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void et_min_max()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ -2, -1, 0, 1, 2 }, y( 5, 1 );
   std::cout << "x =                      " << x << std::endl;
   std::cout << "y =                      " << y << std::endl;
   std::cout << "abs( x ) =               " << abs( x ) << std::endl;
   std::cout << "============================================" << std::endl;
   std::cout << "minimum( x, 0 ) =        " << minimum( x, 0 ) << std::endl;
   std::cout << "maximum( x, 0 ) =        " << maximum( x, 0 ) << std::endl;
   std::cout << "minimum( x, y ) =        " << minimum( x, y ) << std::endl;
   std::cout << "maximum( x, y ) =        " << maximum( x, y ) << std::endl;
   std::cout << "minimum( abs( x ), y ) = " << minimum( abs( x ), y ) << std::endl;
   std::cout << "maximum( abs( x ), y ) = " << maximum( abs( x ), y ) << std::endl;
   std::cout << "============================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET and minimum/maximum functions on the CPU:" << std::endl;
   et_min_max< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Demonstrating ET and minimum/maximum functions on the CUDA GPU:" << std::endl;
   et_min_max< TNL::Devices::Cuda >();
#endif
}
#include <TNL/Containers/Vector.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void et_operators()
{
   using Vector = TNL::Containers::Vector< float, Device >;
   Vector x{ 1, 1, 0, 0 }, y( 4, 2 ),z( 4, 0 );
   std::cout << "=================================" << std::endl;
   std::cout << "x =            " << x << std::endl;
   std::cout << "y =            " << y << std::endl;
   std::cout << "z =            " << z << std::endl;
   std::cout << "=================================" << std::endl;
   std::cout << "x + y =        " << x + y << std::endl;
   std::cout << "x - y =        " << x - y << std::endl;
   std::cout << "x * y =        " << x * y << std::endl;
   std::cout << "x / y =        " << x / y << std::endl;

   z -= y;
   std::cout << "z -=  y -> z = " << z << std::endl;
   z *= y;
   std::cout << "z *=  y -> z = " << z << std::endl;
   z *= -1;
   std::cout << "z *= -1 -> z = " << z << std::endl;
   z /= -y;
   std::cout << "z /= -y -> z = " << z << std::endl;
   z += 3;
   std::cout << "z +=  3 -> z = " << z << std::endl;
   std::cout << "=================================" << std::endl;
}

int main( int argc, char* argv[] )
{
   std::cout << "Demonstrating ET and basic algebraic operators on the CPU:" << std::endl;
   et_operators< TNL::Devices::Host >();

#ifdef __CUDACC__
   std::cout << "Demonstrating ET and basic algebraic operators on the CUDA GPU:" << std::endl;
   et_operators< TNL::Devices::Cuda >();
#endif
}
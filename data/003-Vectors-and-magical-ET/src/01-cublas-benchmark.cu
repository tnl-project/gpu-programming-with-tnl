#include <TNL/Containers/Vector.h>
#include <TNL/Timer.h>
#include <cublas_v2.h>

using namespace std;

const int N = 1<<26;
const int loops = 50;

void cublas_evaluation( float* d_w, const float* d_x, const float* d_y, const float* d_z )
{
   float alpha2 = 2.0f, alpha3 = 3.0f;
   cublasHandle_t handle;
   cublasCreate(&handle);

   // w = x
   cudaMemcpy(d_w, d_x, N * sizeof(float), cudaMemcpyDeviceToDevice);

   // w = w + 2*y = x + 2*y
   cublasSaxpy(handle, N, &alpha2, d_y, 1, d_w, 1);

   // w = w + 3*z = x + 2*y + 3*z
   cublasSaxpy(handle, N, &alpha3, d_z, 1, d_w, 1);

   cublasDestroy(handle);
}

int main()
{
   TNL::Containers::Vector< float, TNL::Devices::Cuda > w( N ), x( N, 1.0 ), y( N, 1.0 ), z( N, 1.0 );

   TNL::Timer timer;
   timer.start();

   for( int i = 0; i < loops; i++ )
      cublas_evaluation( w.getData(), x.getData(), y.getData(), z.getData() );
   timer.stop();

   auto cuBLAS_time = timer.getRealTime();
   cout << "cuBLAS: " << cuBLAS_time << " seconds."  << endl;

   w = 0.0;
   timer.reset();

   timer.start();
   for( int i = 0; i < loops; i++ )
      w += x + 2.0*y + 3.0*z;
   timer.stop();

   auto TNL_time = timer.getRealTime();
   cout << "TNL: " << TNL_time << " seconds." << endl;
   cout << "Speed-up: " << 100.0 * ( 1.0 - TNL_time / cuBLAS_time ) << "%." << endl;

   return EXIT_SUCCESS;
}

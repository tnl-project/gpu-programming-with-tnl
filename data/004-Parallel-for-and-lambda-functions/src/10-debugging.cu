#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

int main(int argc, char **argv)
{
   int size = 10;

   TNL::Algorithms::parallelFor< TNL::Devices::Sequential >( 0, size,
      [=] __cuda_callable__ ( int i ) mutable {
         std::cout << "i = " << i << std::endl;
      });
   return EXIT_SUCCESS;
}
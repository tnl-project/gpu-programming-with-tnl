#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

int main(int argc, char **argv)
{
   int size = 10;

   // Attempt to compute scan on GPU whch does not work ...
   TNL::Containers::Vector<double, TNL::Devices::Cuda> cuda_array(size, 1.0 );
   auto cuda_view = cuda_array.getView();
   TNL::Algorithms::parallelFor< TNL::Devices::Cuda >( 1, size,
      [=] __cuda_callable__ ( int i ) mutable {
         cuda_view[i] += cuda_view[i-1];
      });
   std::cout << "cuda_array = " << cuda_array << std::endl;

   // ... and now the same sequentialy on the host which is ok.
   TNL::Containers::Vector<double, TNL::Devices::Host> host_array(size, 1.0 );
   auto host_view = host_array.getView();
   TNL::Algorithms::parallelFor< TNL::Devices::Sequential >( 1, size,
      [=] __cuda_callable__ ( int i ) mutable {
         host_view[i] += host_view[i-1];
      });
   std::cout << "host_array = " << host_array << std::endl;
   return EXIT_SUCCESS;
}
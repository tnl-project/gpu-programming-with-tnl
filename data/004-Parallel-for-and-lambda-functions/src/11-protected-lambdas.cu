#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

template< typename Vector >
struct VectorInitializer
{
   VectorInitializer( double value) : c( value ){};

   void initVector(Vector& v ) {
      setVectorElements( v );
   }

protected:
   void setVectorElements( Vector& v ) {
      v.forAllElements( [=] __cuda_callable__ ( int i, double& value ) {
         value = c;
      });
   }

   double c = 0.0;
};

int main(int argc, char **argv)
{
   int size = 10;
   TNL::Containers::Vector<double, TNL::Devices::Cuda> v(size);
   VectorInitializer< TNL::Containers::Vector<double, TNL::Devices::Cuda> > vi( 1.0 );
   vi.initVector( v );
   std::cout << "v = " << v << std::endl;
   return EXIT_SUCCESS;
}
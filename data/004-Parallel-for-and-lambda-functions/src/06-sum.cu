#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

int main(int argc, char **argv)
{
   int size = 100;

   TNL::Containers::Vector<double, TNL::Devices::Cuda> input_array(size);
   input_array.forAllElements( [] __cuda_callable__ ( int i, double& value ) {
      value = i;
   });
   TNL::Containers::Vector<double, TNL::Devices::Cuda> output_array(1, 0.0);
   auto in_view = input_array.getView();
   auto out_view = output_array.getView();
   TNL::Algorithms::parallelFor< TNL::Devices::Cuda >( 0, size,
      [=] __cuda_callable__ ( int i ) mutable {
         out_view[0] += in_view[i];
      });

   std::cout << "TNL::sum( input_array ) = " << TNL::sum( input_array ) << std::endl;
   std::cout << "output_array[ 0 ] = " << output_array.getElement( 0 ) << std::endl;
   return EXIT_SUCCESS;
}
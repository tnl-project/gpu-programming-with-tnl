#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>


template< typename Vector >
void parallelForTest( Vector& v)
{
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   auto v_view = v.getView();
   auto f = [=] __cuda_callable__ (Index i) mutable
   {
      v_view[i] = i;
   };
   TNL::Algorithms::parallelFor<Device>(0, v.getSize(), f );
   std::cout << "v = " << v << std::endl;
}

int main(int argc, char **argv)
{
   int size = 10;

   TNL::Containers::Vector<int, TNL::Devices::Host> v_host(size, 0.0);
   std::cout << "Testing parallelFor on CPU:" << std::endl;
   parallelForTest(v_host);

   #ifdef __CUDACC__
   TNL::Containers::Vector<int, TNL::Devices::Cuda> v_gpu(size, 0.0);
   std::cout << "Testing parallelFor on CUDA GPU:" << std::endl;
   parallelForTest(v_gpu);
   #endif

   return EXIT_SUCCESS;
}
#include <iostream>
#include <TNL/Containers/Vector.h>

template <typename LambdaFunction>
void forLoop(int begin, int end, LambdaFunction function)
{
   for (int i = begin; i < end; i++)
      function(i);
}

int main(int argc, char *argv[])
{
   TNL::Containers::Vector<int> v(10, 0);
   std::cout << "Capturing by value:" << std::endl;
   auto f_value = [=](int idx) mutable
   {
      v[idx] = idx;
   };
   forLoop(0, 10, f_value);
   std::cout << v << std::endl;

   std::cout << "Capturing by reference:" << std::endl;
   auto f_reference = [&](int idx) mutable
   {
      v[idx] = idx;
   };
   forLoop(0, 10, f_reference);
   std::cout << v << std::endl;
}

#include <iostream>
#include <TNL/Containers/Vector.h>


template< typename Vector >
void forElementsTest( Vector& v)
{
   using Value = typename Vector::ValueType;
   using Device = typename Vector::DeviceType;
   using Index = typename Vector::IndexType;

   v.forElements( 0, v.getSize()/2, [=] __cuda_callable__ (Index i, Value& v ) mutable {
      v = i;
   } );

   std::cout << "v = " << v << std::endl;
}

int main(int argc, char **argv)
{
   int size = 10;

   TNL::Containers::Vector<int, TNL::Devices::Host> v_host(size, 0.0);
   std::cout << "Testing parallelFor on CPU:" << std::endl;
   forElementsTest(v_host);

   #ifdef __CUDACC__
   TNL::Containers::Vector<int, TNL::Devices::Cuda> v_gpu(size, 0.0);
   std::cout << "Testing parallelFor on CUDA GPU:" << std::endl;
   forElementsTest(v_gpu);
   #endif

   return EXIT_SUCCESS;
}
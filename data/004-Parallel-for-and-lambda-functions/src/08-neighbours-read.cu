#include <iostream>
#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

int main(int argc, char **argv)
{
   int size = 100;

   TNL::Containers::Vector<double, TNL::Devices::Cuda> input_array(size, 1.0 ), output_array(size, 0.0);
   auto in_view = input_array.getView();
   auto out_view = output_array.getView();
   TNL::Algorithms::parallelFor< TNL::Devices::Cuda >( 1, size-1,
      [=] __cuda_callable__ ( int i ) mutable {
         out_view[i] = in_view[i-1] + in_view[i] + in_view[i+1];
      });

   std::cout << "output_array = " << output_array << std::endl;
   return EXIT_SUCCESS;
}
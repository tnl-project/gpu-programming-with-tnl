#include <iostream>

template <typename LambdaFunction>
void forLoop(int begin,
             int end,
             LambdaFunction function)
{
   for (int i = begin; i < end; i++)
      function(i);
}

int main(int argc, char *argv[])
{
   int *data = new int[10];
   auto f = [=](int idx) mutable
   {
      data[idx] = idx;
   };
   forLoop(0, 10, f);
   for (int i = 0; i < 10; i++)
      std::cout << "data[" << i << "] = " << data[i] << std::endl;
   delete[] data;
}

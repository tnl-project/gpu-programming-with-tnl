#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Cuda.h>

int main( int argc, char* argv[] )
{
   // Creating an array of integers on GPU
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array( 10, 1 );

   // Save the array into a file
   cuda_array.save( "array-data" );

   // Load the data into array of integers allocated on the host
   TNL::Containers::Array< int > host_array;
   host_array.load( "array-data" );

   std::cout << "host_array = " << host_array << std::endl;
}
#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Cuda.h>

template< typename Device >
void deviceIndepdentFunction()
{
   // Creating an empty array of integers
   TNL::Containers::Array< int, Device > array;

   // Set size of the array to 10
   array.setSize( 10 );

   // Set all values to zero
   array.setValue( 1 );

   // Print the array
   std::cout << "array = " << array << std::endl;
}

int main( int argc, char* argv[] )
{
   // First execute the device independent function on the CPU
   std::cout << "Executing the device independent function on the CPU:" << std::endl;
   deviceIndepdentFunction< TNL::Devices::Host >();

   // If we compile with the CUDA compiler, execute it even on the GPU.
#ifdef __CUDACC__
   std::cout << "Executing the device independent function on the GPU:" << std::endl;
   deviceIndepdentFunction< TNL::Devices::Cuda >();
#endif
}
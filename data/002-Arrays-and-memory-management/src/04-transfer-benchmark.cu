#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Cuda.h>
#include <TNL/Timer.h>

const int size = 128;

int main( int argc, char* argv[] )
{
   // Creating an array of integers on the host filled with ones
   TNL::Containers::Array< int, TNL::Devices::Host > host_array( size, 1 );

   // Creating an array on the GPU having the same size
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array( size );

   // Copy data using setElement method
   TNL::Timer timer;
   timer.start();
   for( int loops = 0; loops < 1000; loops++ )
      for( int i = 0; i < size; i++ )
         cuda_array.setElement( i, host_array[ i ] );
   timer.stop();
   auto setElement_time = timer.getRealTime() / 1000.0;
   std::cout << "Copying array from host to device using setElement took " << setElement_time <<  " sec." << std::endl;

   // Copy data using assignment operator
   timer.reset();
   timer.start();
   for( int loops = 0; loops < 1000; loops++ )
      cuda_array = host_array;
   timer.stop();
   auto assignment_time = timer.getRealTime() / 1000.0;
   std::cout << "Copying array from host to device using assignement took " << assignment_time <<  " sec." << std::endl;

   std::cout << "Speed-up is " << setElement_time / assignment_time << std::endl;
}
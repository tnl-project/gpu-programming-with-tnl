#include <iostream>
#include <TNL/Containers/ArrayView.h>

int main( int argc, char* argv[] )
{
   // Allocate data using operator new
   int* data = new int[ 10 ];
   {
      // Create an ArrayView wrapping the data
      TNL::Containers::ArrayView< int > view( data, 10 );

      // Use the method setValue to set all elements to number 7
      view.setValue( 7 );
   }
   // The data was not deallocated by the ArrayView, so we can still work with it.
   std::cout << " data[ 0 ] = " << data[ 0 ] << std::endl;

   // Do not forget to free the data
   delete[] data;
}
#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Cuda.h>

int main( int argc, char* argv[] )
{
   // Creating an array of integers on the host with 10 zero elements
   TNL::Containers::Array< int, TNL::Devices::Host > host_array( 10, 0 );

   // Change the first element to one
   host_array.setElement( 0, 1 );

   // Printing all elements using getElement method and operator[]
   for( int i = 0; i < 10; i++ )
      std::cout << "host_array.getElement( " << i << ") = "  << host_array.getElement( i )
                << " host_array[ " << i << " ] = " << host_array[ i ] << std::endl;

   // Creating an array of integers on GPU with 10 zero elements
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array( 10, 0 );

   // Change the first element to one
   cuda_array.setElement( 0, 1 );

   // Printing the first element using getElement method
   std::cout << "cuda_array.getElement( 0 ) = " << cuda_array.getElement( 0 ) << std::endl;

   // Use of index operator for arrays allocated on GPU leads to a crash ...
   std::cout << "cuda_array[ 0 ] = " << cuda_array[ 0 ] << std::endl;
}
#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>

int main( int argc, char* argv[] )
{
   // Creating an array with 10 integer elements on CPU
   TNL::Containers::Array< int > host_array( 10 );
   std::cout << "host_array = " << host_array << std::endl;

   // Creating an empty array of integers on GPU
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array;
   std::cout << "cuda_array = " << cuda_array << std::endl;

   // Creating an array from STL list
   std::list< int > list { 1, 2, 3, 4, 5 };
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array_list( list );
   std::cout << "cuda_array_list = " << cuda_array_list << std::endl;

   // Creating an array from STL vector
   std::vector< int > vector { 6, 7, 8, 9, 10 };
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array_vector( vector );
   std::cout << "cuda_array_vector = " << cuda_array_vector << std::endl;

   // Creating an array using initializer list
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array_init_list{ 11, 12, 13 };
   std::cout << "cuda_array_init_list = " << cuda_array_init_list << std::endl;
}
#include <iostream>
#include <TNL/Containers/Array.h>
#include <TNL/Devices/Cuda.h>

int main( int argc, char* argv[] )
{
   // Creating an empty array of integers on GPU
   TNL::Containers::Array< int, TNL::Devices::Cuda > cuda_array;

   // Set size of the array to 10
   cuda_array.setSize( 10 );

   // Set all values to ones
   cuda_array.setValue( 1 );

   std::cout << "cuda_array = " << cuda_array << std::endl;
}